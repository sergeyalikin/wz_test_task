import { Router } from 'express';
import Sequelize from 'sequelize';
import bodyParser from 'body-parser';
import moment from 'moment';
import request from 'request-promise';

import models, { sequelize } from '../../models';
import { linkFormatCheck, blacklistCheck, favoritesTypeChecking, recordsChecking, errorHandlers, blackList, imageLinkFormatCheck, parametersChecking, sortByChecking, sortDirChecking, filterChecking, dateChecking } from '../../validators/bookmark';

const Op = Sequelize.Op
const router = Router();

var urlencodedParser = bodyParser.urlencoded({
    extended: false
});

/** 
@apiDescription Данные о закладках в БД
@apiSuccessExample SUCCESS:
HTTP/1.1 200 OK
{
    "length": 2,
    "data": [
        {
            "guid": "cd1331c3-5fcf-4064-9ebf-a4ef9002e77c",
            "link": "https://mail.google.com/",
            "createdAt": "2019-12-01",
            "description": "Гугл почта",
            "favorites": false
        },
        {
            "guid": "d5b47bea-050c-4978-aa80-196c907d234b",
            "link": "https://mail.yandex.ru/",
            "createdAt": "2019-12-01",
            "description": "Почта",
            "favorites": true
        }
    ]
}

@apiErrorExample ALL EXAMPLES:
HTTP/1.1 400 Bad Request
{
    "errors": [
        {
            "code": "BOOKMARKS_INVALID_SORT_BY",
            "description": "Invalid sort_by value"
        }
    ]
}
{
    "errors": [
        {
            "code": "BOOKMARKS_INVALID_SORT_DIR",
            "description": "Invalid sort_dir value"
        }
    ]
}
{
    "errors": [
        {
            "code": "BOOKMARKS_INVALID_FILTER",
            "description": "Invalid filter value"
        }
    ]
}
{
    "errors": [
        {
            "code": "BOOKMARKS_INVALID_DATE",
            "description": "Invalid date format"
        }
    ]
}
HTTP/1.1 404 Bad Request
{
    "errors": [
        {
            "code": "Not found",
            "description": "No such entries"
        }
    ]
}
*/

router.get('/bookmarks', async (req, res) => {
    try {
        const request = req.query
        let offset = request.offset ? request.offset : 0
        let limit = request.limit ? request.limit : 50
        let sort_by = request.sort_by ? request.sort_by : "createdAt"
        let sort_dir = request.sort_dir ? request.sort_dir : "ASC"
        let filter_from = request.filter_from ? request.filter_from : "2010-01-01"
        let now = moment().format("Y-M-D");
        let filter_to = request.filter_to ? request.filter_to : now
        if (request.filter_value !== 'true' && request.filter_value !== 'false' && typeof request.filter_value !== 'undefined') {
            res.status(400).json(favoritesTypeChecking)
            return
        }
        let filter_value = (request.filter_value || typeof request.filter_value === 'undefined') ? (request.filter_value == 'true') : false
        if ((request.filter_value) && (typeof request.filter === 'undefined' || request.filter === 'createdAt')) {
            res.status(400).json(parametersChecking)
        }
        if ((request.filter === "createdAt" || typeof request.filter === "undefined")) {
            filter_value = [true, false]
        } else if (request.filter === "favorites") {
            filter_value = request.filter_value ? [request.filter_value] : [true]
        }
        if (sort_by !== "favorites" && sort_by !== "createdAt") {
            res.status(400).json(sortByChecking)
            return
        }
        if (sort_dir !== "ASC" && sort_dir !== "DESC") {
            res.status(400).json(sortDirChecking)
            return
        }
        if (request.filter !== "favorites" && request.filter !== "createdAt" && typeof request.filter !== 'undefined') {
            res.status(400).json(filterChecking)
            return
        }
        if (!moment(filter_from, 'Y-M-D').isValid() || !moment(filter_to, 'Y-M-D').isValid()) {
            res.status(400).json(dateChecking)
            return
        }
        const results = await models.bookmarks.findAll({
            limit,
            offset,
            order: [
                [sort_by, sort_dir]
            ],
            attributes: ['guid', 'link', 'createdAt', 'description', 'favorites'],
            where: {
                createdAt: {
                    [Op.between]: [filter_from, filter_to]
                },
                favorites: {
                    [Op.in]: filter_value
                }
            },
        })
        const count = await models.bookmarks.count({
            where: {
                createdAt: {
                    [Op.between]: [filter_from, filter_to]
                },
                favorites: {
                    [Op.in]: filter_value
                }
            }
        })
        if (results.length != 0) {
            res.status(200).json({
                length: count,
                data: results,
            })
        } else {
            res.status(404).json(recordsChecking)
        }
    } catch (error) {
        console.log(error);
        res.status(400).json(errorHandlers(error))
    }
});

/**
@apiDescription Создание закладки в БД
@apiSuccessExample SUCCESS:
HTTP/1.1 200 OK
{
    "data": {
        "guid": "b8c94469-749a-4797-9d9d-77d9a31c9944",
        "createdAt": "2019-12-02"
    }
}
@apiErrorExample ALL EXAMPLES:
HTTP/1.1 400 Bad Request
{
    "errors": [
        {
            "code": "BOOKMARKS_INVALID_LINK",
            "description": "Invalid link"
        }
    ]
}
{
    "errors": [
        {
            "code": "BOOKMARKS_BLOCKED_DOMAIN",
            "description": "Invalid link"
        }
    ]
}
{
    "errors": [
        {
            "code": "BOOKMARKS_INVALID_FAVORITES",
            "description": "No boolean value"
        }
    ]
}
*/

router.post('/bookmarks', urlencodedParser, async (req, res) => {
    try {
        if (!(req.body.link.startsWith('http://') || req.body.link.startsWith('https://'))) {
            res.status(400).json(linkFormatCheck)
            return
        }
        let forbiddenItem = blackList.find((elem) => {
            return req.body.link.includes(elem)
        });
        if (forbiddenItem) {
            res.status(400).json(blacklistCheck)
            return
        }
        let imageLink = req.body.image ? req.body.image : ''
        if (!(imageLink.startsWith('https://')) && imageLink !== '') {
            res.status(400).json(imageLinkFormatCheck)
            return
        }
        let newBookmarks = {
            link: req.body.link,
            image: req.body.image,
            title: req.body.title,
            description: req.body.description,
            favorites: req.body.favorites === undefined ? true : req.body.favorites,
        }
        if (typeof newBookmarks.favorites !== 'boolean') {
            res.status(400).json(favoritesTypeChecking)
            return
        }
        let newBookmarksDB = await sequelize.models.bookmarks.create(newBookmarks)
        res.status(200).json({
            data: {
                guid: newBookmarksDB.guid,
                createdAt: newBookmarksDB.createdAt
            }
        })
    } catch (error) {
        console.log(error);
        res.status(400).json(errorHandlers(error))
    }
})

/** 
@apiDescription Изменение закладки в БД
@apiSuccessExample SUCCESS:
HTTP/1.1 200 OK
{
    "send": "Record changed"
}
@apiErrorExample ALL EXAMPLES:
HTTP/1.1 404 Bad Request
{
    "errors": [
        {
            "code": "Not found",
            "description": "No such entries"
        }
    ]
}
HTTP/1.1 400 Bad Request
{
    "errors": [
        {
            "code": "BOOKMARKS_BLOCKED_DOMAIN",
            "description": "Invalid link"
        }
    ]
}
{
    "errors": [
        {
            "code": "BOOKMARKS_INVALID_LINK",
            "description": "Invalid link"
        }
    ]
}
{
    "errors": [
        {
            "code": "BOOKMARKS_INVALID_FAVORITES",
            "description": "No boolean value"
        }
    ]
}
*/

router.patch('/bookmarks/:guid', urlencodedParser, async (req, res) => {
    try {
        const results = await models.bookmarks.findAll({
            raw: true,
            where: {
                guid: req.params.guid
            },
        })
        if (results.length === 0) {
            res.status(404).json(recordsChecking)
            return
        }
        let link = req.body.link ? req.body.link : results[0].link
        let imageLink = req.body.image ? req.body.image : ''
        let description = req.body.description ? req.body.description : results[0].description
        let favorites = typeof req.body.favorites === 'undefined' ? results[0].favorites : req.body.favorites
        let now = moment().format('Y-M-D');
        if (!(link.startsWith('http://') || link.startsWith('https://'))) {
            res.status(400).json(linkFormatCheck)
            return
        }
        if (!(imageLink.startsWith('https://')) && imageLink !== '') {
            res.status(400).json(imageLinkFormatCheck)
            return
        }
        let forbiddenItem = blackList.find(function (elem) {
            return link.includes(elem)
        });
        if (forbiddenItem) {
            res.status(400).json(blacklistCheck)
            return
        }
        if (typeof favorites !== 'boolean') {
            res.status(400).json(favoritesTypeChecking)
            return
        }
        await sequelize.models.bookmarks.update({
            link: link,
            description: description,
            favorites: favorites,
            title: req.body.title,
            image: req.body.image,
            updatedAt: now
        }, {
            where: {
                guid: req.params.guid
            }
        })
        res.status(200).json({
            send: "Record changed"
        })
    } catch (error) {
        console.log(error);
        res.status(400).json(errorHandlers(error))
    }
})

/**
@apiDescription Удаление закладки в БД
@apiSuccessExample SUCCESS:
HTTP/1.1 200 OK
{
    "send": "Record destroyed"
}
@apiErrorExample ALL EXAMPLES:
HTTP/1.1 404 Bad Request
{
    "errors": [
        {
            "code": "Not found",
            "description": "No such entries"
        }
    ]
}
*/

router.delete('/bookmarks/:guid', urlencodedParser, async (req, res) => {
    try {
        let results = await sequelize.models.bookmarks.destroy({
            where: {
                guid: req.params.guid
            }
        })
        if (results === 0) {
            res.status(404).json(recordsChecking)
            return
        }
        res.status(200).json({
            send: "Record destroyed"
        })
    } catch (error) {
        console.log(error);
        res.status(400).json(errorHandlers(error))
    }
})

/**
@apiDescription Дополнительная информация о закладке
@apiSuccessExample SUCCESS:
HTTP/1.1 200 OK
{
    "og": {
        "title": "Тайтл",
        "image": null,
        "description": "Эпл"
    },
    "whois": {
        "response": {
            "schema": "http://",
            "url": "/",
            "base": "apple.ru",
            "domain": "apple.ru",
            "zone": "ru",
            "creation": "13.10.1996",
            "paid": "01.11.2020",
            "free": "02.12.2020",
            "age": 23,
            "upd": "07.12.2019",
            "limit": 0
        }
    }
}
@apiErrorExample ALL EXAMPLES:
HTTP/1.1 404 Bad Request
{
    "errors": [
        {
            "code": "Not found",
            "description": "No such entries"
        }
    ]
}
*/

router.get('/bookmarks/:guid', async (req, res) => {
    try {
    let result = await sequelize.models.bookmarks.findOne({
        attributes: ['link', 'title', 'image', 'description'],
        raw: true,
        where: {
            guid: req.params.guid
        }
    })
    if (result === null) {
        res.status(404).json(recordsChecking)
        return
    }
    const options = {
        method: 'GET',
        uri: `https://htmlweb.ru/analiz/api.php?whois&url=${result.link}&json`,
        json: true
    }
    request(options)
        .then((response) => {
            res.json({
                og: {
                    title: result.title,
                    image: result.image,
                    description: result.description,
                },
                whois: {
                    response: {
                        schema: response.schema,
                        url: response.url,
                        base: response.base,
                        domain: response.domain,
                        zone: response.zone,
                        creation: response.creation,
                        paid: response.paid,
                        free: response.free,
                        age: response.age,
                        upd: response.upd,
                        limit: response.limit
                    }
                }
            })
            return
        })
        .catch((err) => {
            res.status(400).json(errorHandlers(err))
            return
        })
    } catch (error) {
        console.log(error);
        res.status(400).json(errorHandlers(error))
    }
})

export default router;