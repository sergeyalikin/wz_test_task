export const linkFormatCheck = {
  errors: [
    {
      code: 'BOOKMARKS_INVALID_LINK',
      description: 'Invalid link'
    }
  ]
}

export const imageLinkFormatCheck = {
  errors: [
    {
      code: 'BOOKMARKS_INVALID_IMAGE_LINK',
      description: 'Invalid image link'
    }
  ]
}

export let blackList = ['yahoo.com', 'socket.io']

export const blacklistCheck = {
        errors: [
          {
            code: 'BOOKMARKS_BLOCKED_DOMAIN',
            description: 'Invalid link'
          }
        ]
      }

export const favoritesTypeChecking = {
      errors: [
        {
          code: 'BOOKMARKS_INVALID_TYPE',
          description: 'No boolean value'
        }
      ]
    }



export const recordsChecking = {
      errors: [
        {
          code: 'Not found',
          description: 'No such entries'
        }
      ]
    }
export const errorHandlers = (error) => {
  return {
    errors: {
      backend: "Can't get list of bookmarks",
      error: error
    }
  }
}

export const parametersChecking = {
  errors: [
      {
          code: 'PARAMETERS_ARE_NOT_SET_CORRECTLY',
          description: 'Check if parameters filter and filter_value are correct'
      }
  ]
}

export const sortByChecking = {
  errors: [
      {
          code: 'BOOKMARKS_INVALID_SORT_BY',
          description: 'Invalid sort_by value'
      }
  ]
}

export const sortDirChecking = {
  errors: [
      {
          code: 'BOOKMARKS_INVALID_SORT_DIR',
          description: 'Invalid sort_dir value'
      }
  ]
}

export const filterChecking = {
  errors: [
      {
          code: 'BOOKMARKS_INVALID_FILTER',
          description: 'Invalid filter value'
      }
  ]
}

export const dateChecking = {
  errors: [
      {
          code: 'BOOKMARKS_INVALID_DATE',
          description: 'Invalid date format'
      }
  ]
}