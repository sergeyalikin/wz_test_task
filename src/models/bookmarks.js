'use strict'

module.exports = (sequelize, DataTypes) => {
    const bookmarks = sequelize.define('bookmarks', {
        guid: {
            allowNull: false,
            primaryKey: true,
            type: DataTypes.UUID,
            defaultValue: DataTypes.UUIDV4
        },
        link: {
            type: DataTypes.STRING(256),
            allowNull: false,
        },
        title: {
            type: DataTypes.STRING(256),
            allowNull: true,
        },
        image: {
            type: DataTypes.STRING(256),
            allowNull: true,
        },
        createdAt: {
            type: DataTypes.DATEONLY,
            allowNull: false,
            defaultValue: DataTypes.NOW,
        },
        updatedAt: {
            type: DataTypes.DATEONLY,
            allowNull: false,
            defaultValue: DataTypes.NOW,
        },
        description: {
            type: DataTypes.STRING,
        },
        favorites: {
            type: DataTypes.BOOLEAN,
        },
    })
    return bookmarks
};